import React, {useState} from 'react'; 
import TaskList from './TaskList';

function App(){
  const [tasks, setTasks] = useState([]);
  const [newTasks, setNewTasks] = useState('');

  const handleAddTask = () => {
    if (newTasks.trim() !== ''){
      setTasks([...tasks, {title: newTasks, completed: false}]);
      setNewTasks('');
    }
  };

  const handleCompleteTask = (index) => {
    const updateTasks = [...tasks];
    updateTasks[index].completed = true;
    setTasks(updateTasks);
  };

  const handleDeleteTask = (index) => {
    const updateTasks = [...tasks];
    updateTasks.splice(index, 1);
    setTasks(updateTasks);
  };

  return (
    <div>
      <h1> List of Tasks</h1>

      <input type="text" placeholder="add Task" value={newTasks} onChange={(e) => setNewTasks(e.target.value)}/>
      <button onClick={handleAddTask}>Add Task</button>

      <TaskList tasks={tasks} onComplete={handleCompleteTask} onDelete={handleDeleteTask}/>
    </div>
  )
}


export default App;
