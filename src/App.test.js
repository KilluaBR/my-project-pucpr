import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

test('renders App component', () => {
  const { getByText } = render(<App />);
  const headerElement = getByText(/List of Tasks/i);
  expect(headerElement).toBeInTheDocument();
});

test('adds a new task', () => {
  render(<App />);
  const inputElement = screen.getByPlaceholderText('add Task');
  const addButtonElement = screen.getByText('Add Task');

  fireEvent.change(inputElement, { target: { value: 'New Task' } });
  fireEvent.click(addButtonElement);

  const taskElement = screen.getByText('New Task');
  expect(taskElement).toBeInTheDocument();
});

test('completes a task', () => {
  render(<App />);
  const inputElement = screen.getByPlaceholderText('add Task');
  const addButtonElement = screen.getByText('Add Task');

  fireEvent.change(inputElement, { target: { value: 'New Task' } });
  fireEvent.click(addButtonElement);

  const completeButtonElement = screen.getByText('Completed');
  fireEvent.click(completeButtonElement);

  const completedTaskElement = screen.getByText('Completed');
  expect(completedTaskElement).toBeInTheDocument();
});

test('deletes a task', () => {
  render(<App />);
  const inputElement = screen.getByPlaceholderText('add Task');
  const addButtonElement = screen.getByText('Add Task');

  fireEvent.change(inputElement, { target: { value: 'New Task' } });
  fireEvent.click(addButtonElement);

  const deleteButtonElement = screen.getByText('Deleted');
  fireEvent.click(deleteButtonElement);

  const taskElement = screen.queryByText('New Task');
  expect(taskElement).toBeNull();
});
