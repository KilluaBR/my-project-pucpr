import React from 'react';

function TaskList(props){
    return (
        <ul>
            {props.tasks.map((task, index) =>(
                <li key={index} className={task.completed ? 'completed' : ''}>
                    {task.title}
                    <button onClick={() => props.onComplete(index)}>Completed</button>
                    <button onClick={() => props.onDelete(index)}>Deleted</button>
                </li>
            ))}
        </ul>
    )
}
export default TaskList;